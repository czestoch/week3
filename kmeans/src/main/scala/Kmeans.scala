import org.apache.log4j.Logger
import org.apache.log4j.Level
import org.apache.spark.sql.SparkSession
import org.apache.spark.rdd._
import scala.tools.nsc.io.File

object KmeansSpark {
//------------------------------------------------------------------------------
    def main(args: Array[String]) = {
        Logger.getLogger("org").setLevel(Level.ERROR)
        Logger.getLogger("akka").setLevel(Level.ERROR)

        val spark = SparkSession.builder
                                 .appName("Kmeans Movies").getOrCreate()
        val (features, knn) = loadData(spark)
        run(10, features, knn, 10)
        spark.stop()
    }

    type POINT = Vector[Double]
//------------------------------------------------------------------------------
    def run(k: Int, features: RDD[(Int, POINT)],
            knn: RDD[(Int, Vector[Int])], max_iterations: Int) {
        var i = 0
        var centres = features.take(k)
                              .map(_._2) // take only features, ignore movie id
        var terminate = false
        do {
            val result = mapReduce(centres, features)
            terminate = terminationCriterion(centres, result, knn, features)
            centres = result
            i = i + 1
            if (i >= max_iterations) return
        } while (!terminate)
    }

//------------------------------------------------------------------------------
//  MapReduce
//------------------------------------------------------------------------------
    def mapReduce(centres: Array[POINT],
                  points: RDD[(Int, POINT)]): Array[POINT] = {
        points.map(p => mapper(p, centres))
              .groupByKey()
              .map(reducer)
              .collect
    }

//------------------------------------------------------------------------------
    def mapper(point: (Int, POINT),
               centres: Array[POINT]) = {
        val closestCentre =
                centres.map(c => (c, distance(point._2, c)))
                       .sortBy(_._2)    // sorts by distance
                       .apply(0)._1     // takes the closest, ignore distance
        (closestCentre, point)          // kv pair: closest centre and the point
    }

//------------------------------------------------------------------------------
    def reducer(grouped: (POINT, Iterable[(Int, POINT)])): POINT = {
        val points = grouped._2         // ignore centre
                            .map(_._2)  // ignore movieid
        points.transpose
              .map(v => v.sum.toDouble / v.size)
              .toVector
    }

//------------------------------------------------------------------------------
//  Functions about termination criteria
//------------------------------------------------------------------------------
    def averageDistanceAcrossIterations(prev: Array[POINT],
                                        now:  Array[POINT]) = {
        val distances = prev.zip(now).map(p => distance(p._1, p._2))
        distances.sum / distances.size
    }

    def similarityToKnn(centres: Array[POINT], knn: RDD[(Int, Vector[Int])],
                        feats: RDD[(Int, POINT)]) = {
        val closest = feats.map(p => mapper(p, centres))
                       .groupByKey().cache // (c: POINT, Iterable[(Int, Point)])
        val intersections =
               feats.join(knn) // (movie: Int, (POINT, knn: Vector[Int]))
                    .map(kv => (mapper((kv._1,kv._2._1), centres)._1, kv._2._2))
                              // (closestcentre: POINT, KNN)
                    .join(closest) //(POINT,(Vector[Int],Iterable[(Int,POINT)])
                    .map(kv => kv._2._1.toSet
                                 .intersect(kv._2._2.map(_._1).toSet).size)
        intersections.sum.toDouble / intersections.count // average
    }

    def terminationCriterion(prev: Array[POINT], now:  Array[POINT],
                             knn: RDD[(Int, Vector[Int])], 
                             feats: RDD[(Int, POINT)]) = {
        val dist = averageDistanceAcrossIterations(prev, now)
        val sim = similarityToKnn(now, knn, feats)
        println(s"Average distance: $dist\tSimilarity to KNN: $sim")
        false
    }

//------------------------------------------------------------------------------
//  Auxiliary functions
//------------------------------------------------------------------------------
    def distance(p: POINT, q: POINT) = {
        val squareOfDiff = p.zip(q).map(p => (p._1 - p._2) * (p._1 - p._2))
        math.sqrt(squareOfDiff.sum)
    }

//------------------------------------------------------------------------------
    def loadData(spark: SparkSession) = {
        import spark.implicits._
        val datafile =
                  "/cs449/week3-exercises/movie-feats-rank36-lamb0.1-iter20.dat"
        val features = spark.read.options(Map("header" -> "false"))
                      .csv(datafile).rdd
                      .map(r => (r.getString(0).toInt,
                                 (1 until r.length)
                                        .map(r.getString(_).toDouble).toVector))

        val knndatafile = "/cs449/week2-exercises/knnMF36-ml-25m.txt"
        val knn = spark.read.options(Map("header" -> "false"))
                      .csv(knndatafile).rdd
                      .map(r => (r.getString(0).toInt,
                                 (1 until r.length)
                                        .map(r.getString(_).toInt).toVector))

        (features.cache(), knn.cache())
    }

//------------------------------------------------------------------------------
}
